import requests 
import json
import pandas as pd
import numpy as np
import re
import time
import datetime
from datetime import datetime, timedelta
import pytz
import warnings
import spacy
from spacy.matcher import PhraseMatcher
from collections import Counter
from IPython import get_ipython

try:
    nlp = spacy.load('en_core_web_sm')
except OSError:
    import spacy.cli
    spacy.cli.download("en_core_web_sm")
    nlp = spacy.load('en_core_web_sm')


page_specific_options = ['andtext', 'ortext', 'phrasetext', 'proxText', 'proximityValue', 'dateFilterType', 'date1', 'date2', 'sequence']
title_specific_options = ['terms', 'county', 'city', 'year1', 'year2', 'frequency', 'language', 'ethnicity', 'labor', 'material_type']
crossover_options = ['state', 'lccn']

# To search the database
class Search:
    def __init__(self, search_type='page', **params):
        self.search_type = search_type
        self.url_path = f"https://chroniclingamerica.loc.gov/search/{search_type}s/results/"
        self.search_url = ''
        params.setdefault('format', 'json')
        params.setdefault('rows', 50)
        self.params = params
        self.search_terms = []
        self.first_page = {}
        self.data_dict = {}

    def checking_params(self):
        if self.search_type == 'title':
            page_params = [p for p in self.params.keys() if p in page_specific_options]
            if len(page_params) > 0:
                print(f"Search type chosen is a title search. The search parameters provided include {', '.join(page_params).upper()}, which are only availiable when running a page search. These will be ignored by Chronicling America in this search. Please check search type and available parameters, update and re-run your search.")
        if self.search_type == 'page': 
            date_params = ['dateFilterType', 'date1', 'date2']
            title_params = [p for p in self.params.keys() if p in title_specific_options]
            if len(title_params) > 0:
                print(f"Search type chosen is a page search. The search parameters provided include {', '.join(title_params).upper()}, which are only availiable when running a title search. These will be ignored by Chronicling America in this search. Please check search type and available parameters, update and re-run your search.")
            if 'proximityValue' in self.params and 'proxText' not in self.params:
                print('proxText value not included in search, so proximityValue included will be disregarded by Chronicling America. If you want to limit your search terms to the set proximity, please include the search terms using the argument "proxText" and re-run search.')
            if any(i in self.params.keys() for i in date_params) and not all(i in self.params.keys() for i in date_params):
                print(f"Search includes arguments {', '.join([i for i in date_params if i in self.params.keys()])} for date but not {', '.join([i for i in date_params if i not in self.params.keys()]).upper()}, so Chronicling America will not include the date limitations in the search. Please update the search and re-run.")
            
    def run_search(self, page=1):
        if self.search_url == '':
            self.checking_params()
        
        params = self.params
        params.update(page=page)
        
        self.search_terms = [val for (key, val) in params.items() if re.search('(and|or|prox|phrase)text', key)]

        self.expanded_search_terms = (
                self.search_terms + 
                [word.replace(' ', '-') for word in self.search_terms] + 
                [word.replace(' ', '') for word in self.search_terms] + 
                [word.lower() for word in self.search_terms] + 
                [word.upper() for word in self.search_terms] + 
                [word.title() for word in self.search_terms] + 
                [word.capitalize() for word in self.search_terms]
                )

        self.expanded_search_terms = sorted(list(set(self.expanded_search_terms)))

        response = requests.get(self.url_path, params=self.params, headers={'User-Agent': 'Chrome/102.0.0.0'})
    
        if response.status_code == 504:
            print('timeout errors, reducing page size...')
            params.update(rows=20)
            response = requests.get(self.url_path, params=self.params, headers={'User-Agent': 'Chrome/102.0.0.0'})

        if self.search_url == '':
            self.search_url = response.url
            self.first_page = response.json()
        
        return response.json()
    
    def check_if_search_previously_run(self):
        if self.search_url == '':
            print('running search...')
            self.run_search()

    def result_count(self):   
        self.check_if_search_previously_run()

        print(f"{self.first_page['totalItems']:,} results found")
        
    def get_link(self):
        """function to see the search as it appears online in a Chronicling America search result"""
        self.check_if_search_previously_run()
        
        url = self.search_url
        url = url.replace('&format=json', '')
        print(f"The link for this search is {url}")

    def next_page(self, response):
        """function to get details of the next page in a Chronicling America search result"""
        if response['endIndex'] < response['totalItems']:
            page_number = int(response['endIndex']/response['itemsPerPage'])
            next_page_number = page_number+1
            next_page = self.run_search(page=next_page_number)
            return next_page
        else:
            return False

    def result_tracker(self, response):
        print(f"{response['endIndex']} of {response['totalItems']} results retrieved.")
        
    
    def apply_nlp(self, concord=True, sentence=True, ner=True, pos=True, pos_choice=['PROPN'], **kwargs):
        matcher = PhraseMatcher(nlp.vocab)
        
        patterns = [nlp(text) for text in self.expanded_search_terms]

        matcher.add('SearchMatch', patterns)

        length = len(self.data_dict['results'])
        count = 1
        for item in self.data_dict['results']:
            if count == int((10 * length) / 100.0):
                print('#          10% complete')
            elif count == int((20 * length) / 100.0):
                print('##         20% complete')
            elif count == int((30 * length) / 100.0):
                print('###        30% complete')
            elif count == int((40 * length) / 100.0):
                print('####       40% complete')
            elif count == int((50 * length) / 100.0):
                print('#####      50% complete')
            elif count == int((60 * length) / 100.0):
                print('######     60% complete')
            elif count == int((70 * length) / 100.0):
                print('#######    70% complete')
            elif count == int((80 * length) / 100.0):
                print('########   80% complete')
            elif count == int((90 * length) / 100.0):
                print('#########  90% complete')

            try:
                doc = nlp(item['ocr_eng'])
                
                found_matches = matcher(doc)
                matches = [doc[match_start:match_end].text for match_id, match_start, match_end in found_matches]
                    
                item['counts'] = {match:count for match, count in Counter(matches).items()}
                unique_matches = list(set(matches))
                
                if concord==True: 
                    item['concordance'] = {}
                    for term in unique_matches:
                        item['concordance'][term] = [doc[match_start-5:match_end+5].text + '\n\n' for match_id, match_start, match_end in found_matches if doc[match_start:match_end].text == term]
                        
                if sentence==True:
                    item['sentence'] = {}
                    sentences = [sent for sent in doc.sents]
                    
                    for term in unique_matches:
                        item['sentence'][term] = [sentence.text + '\n\n' for sentence in sentences for match_id, match_start, match_end in found_matches if doc[match_start:match_end].text == term and match_start >= sentence.start and match_end <= sentence.end]

                if ner==True:
                    ner_types = ['NORP', 'FAC', 'ORG', 'GPE', 'LOC', 'PRODUCT', 'EVENT', 'WORK_OF_ART', 'LAW', 'LANGUAGE', 'DATE', 'TIME', 'PERCENT', 'MONEY', 'QUANTITY', 'ORDINAL', 'CARDINAL']
                    ner_long_names = {
                                    'PERSON': 'Person',
                                    'NORP': 'Nationality/Religious or Political group',            
                                    'FAC': 'Facility',
                                    'ORG': 'Organisation',
                                    'GPE': 'Geopolitical area',
                                    'LOC': 'Location',
                                    'PRODUCT': 'Product',
                                    'EVENT': 'Event',
                                    'WORK_OF_ART': 'Work of Art',
                                    'LAW': 'Document made into law',
                                    'LANGUAGE': 'Language',
                                    'DATE': 'Date',
                                    'TIME': 'Time',
                                    'PERCENT': 'Percentage',
                                    'MONEY': 'Monetary value',
                                    'QUANTITY': 'Quantity',
                                    'ORDINAL': 'Ordinal number',
                                    'CARDINAL': 'Other number'
                                    }

                    item['named_entities'] = {type: [ent.text for ent in doc.ents if ent.label_ == type] for type in ner_types}
                    item['named_entities'] = dict((ner_long_names[key], sorted(value)) for (key, value) in item['named_entities'].items())

                if pos==True:
                    part_of_speech = pos_choice
                    item['parts_of_speech'] = {type: [token.text for token in doc if token.pos_==type] for type in part_of_speech}

            except:
                pass
            
            count += 1
            
        print('########## complete')
        
        return self.data_dict  
    
    
    def gather_results(self, nlp=True, links=True, **kwargs):
        """function to run a search (if not yet done) and collect all results into a single list. 
        This list is put in a dictionary as the value for 'results', alongside a results count."""
        
        """function to collect all the search results into a single list, contained within a dictionary that also incudes the 
        create a dataframe of all items in a Chronicling America page or title search"""
        self.check_if_search_previously_run()
        
        response = self.first_page
        data = []
        
        time_count = timedelta(seconds = ((response['totalItems']/50) * 30) + response['totalItems'])
        est_finish = datetime.now() + time_count
        local_est_finish = pytz.timezone('Australia/Melbourne').localize(est_finish)
        print(f"Estimated processing time: {str(time_count)}.\nEstimated processing finish time {local_est_finish.strftime('%I:%M%p, %d %B %Y')}, {local_est_finish.tzname()}.\n")


        while response:
            self.result_tracker(response)
            data+= response['items']
            response = self.next_page(response)
            # time.sleep(0.2)

        self.data_dict = {'result_count': len(data), 'results': data}
        
        search = self.search_url
        for item in self.data_dict['results']:
            item['search_url'] = search.replace('&format=json', '').replace('&rows=100', '').replace('&page=1', '')
            item['search_terms'] = self.search_terms
            item['search_terms_expanded'] = self.expanded_search_terms

            if links==True:
                item['page_url'] = item['url'].replace('.json', '/') + '#words=' + '+'.join(self.expanded_search_terms).replace(' ', '+')
                item['pdf_url'] = item['url'].replace('.json', '.pdf')
                item['jpg_url'] = item['url'].replace('.json', '.jp2')
        
        if nlp == True:
            print('adding natural language processing...')
            self.data_dict = self.apply_nlp(**kwargs)

        return self.data_dict


    def search_to_frame(self, clean=True, **kwargs):
        """function combining run_search() and items_to_frame, 
            to run a search and put the results in a dataframe in one step"""
        if self.data_dict == {}:
            print('running search...')
            self.gather_results(**kwargs)
        
        print('Converting search results to a dataframe...')
        response = self.data_dict
        df = pd.json_normalize(response, record_path='results')  
        
        if clean == True: 
            print('Cleaning dataframe...')
            df = dataframe_cleaner(df)
        return df
    
    def list_newspaper_lccns(self):  
        """function to create list of all LCCNs and dictionary of all paper titles and LCCNs 
            in a Chronicling America title search"""
        if self.search_type == 'title':
            self.check_if_search_previously_run()
            
            lccns = []
            papers = {}
            response = self.run_search()

            while response:   
                for item in response['items']:
                    lccns.append(item['lccn'])
                    papers[item['title']] = item['lccn']

                self.result_tracker(response)
                response = self.next_page(response)
                time.sleep(0.2)

            print('\nsearched papers - LCCNs only:\n', lccns)
            print('\nsearched papers and their LCCN:\n', papers)
        else:
            print('Search is a page search. To list newspaper LCCNs, include search_type="title", confirm using title search parameters, and re-run search.')
          
        
# To tidy the dataframe
def reorder_columns(df):
    input_df = df.copy()

    intro_columns = ['title_normal', 'place_of_publication']
    date_columns = ['verbose date', 'date', 'weekday', 'day', 'month', 'year', 'date order']
    count_columns = [col for col in input_df.columns if re.match('counts.', col)]
    concordance_columns = [col for col in input_df.columns if re.match('concordance.', col)]
    sentence_columns = [col for col in input_df.columns if re.search('sentence.', col)]
    text_column = ['ocr_eng', 'sentences']
    issue_columns = ['sequence', 'page', 'section_label', 'edition', 'edition_label']
    newspaper_columns = ['title', 'alt_title', 'frequency', 'place', 'city', 'county', 'state', 
                         'country', 'start_year', 'end_year', 'publisher', 'lccn', 'language'] 
    nlp_columns = [col for col in input_df.columns if re.search('named_entities.', col)]
    pos_columns = [col for col in input_df.columns if re.search('parts_of_speech.', col)]
    text_detail_columns = ['subject', 'note', 'type']
    url_columns = [col for col in input_df.columns if re.search(r'[\s_]url$', col, re.IGNORECASE)]
    metadata_columns = ['batch', 'id']
    
    standard_columns = (intro_columns + date_columns + count_columns + concordance_columns + 
                        sentence_columns + text_column + issue_columns + newspaper_columns + 
                        nlp_columns + pos_columns + text_detail_columns + url_columns + metadata_columns)

    extra_columns = [col for col in input_df.columns if col not in standard_columns]
    
    order = standard_columns + extra_columns
    
    columns = [col for col in order if col in input_df.columns]
    
    return input_df[columns]


def format_date(df, date_split=True):
    input_df = df.copy()
    if input_df['date'].dtype != 'datetime64[ns]':
        try:
            with warnings.catch_warnings():
                # Setting values in-place is fine, ignore the warning in Pandas >= 1.5.0
                # This can be removed, if Pandas 1.5.0 does not need to be supported any longer.
                # See also: https://stackoverflow.com/q/74057367/859591
                warnings.filterwarnings(
                    "ignore",
                    category=FutureWarning,
                    message=(
                        ".*will attempt to set the values inplace instead of always setting a new array. "
                        "To retain the old behavior, use either.*"
                    ),
                )
                input_df.loc[:, 'date'] = pd.to_datetime(input_df['date'],format='%Y%m%d')
                
        except ValueError:
            print('error in date formatting')
     
    date = input_df.columns.get_loc('date')
    
    if 'verbose date' not in input_df.columns:
        input_df.insert(date+1, 'verbose date', pd.Series(str(x) + " " + str(y) + " " + str(z) for x,y,z in zip(
                                                                                    input_df['date'].dt.day,
                                                                                    input_df['date'].dt.month_name(),
                                                                                    input_df['date'].dt.year)
                                                                                       ))
                  
                  
    if date_split==True and 'date order' not in input_df.columns:
        input_df = input_df.sort_values(by=['date'])
        input_df.insert(date+2, 'date order', range(1, 1 + len(input_df)))
        input_df = input_df.sort_index()
    
    if date_split==True and 'weekday' not in input_df.columns:
        input_df.insert(date+2, 'year', input_df['date'].dt.year)
        input_df.insert(date+2, 'month', input_df['date'].dt.month)
        input_df.insert(date+2, 'day', input_df['date'].dt.day)
        input_df.insert(date+2, 'weekday', input_df['date'].dt.day_name())
           
    return input_df


def standard_place_publication(df):
    input_df = df.copy()
    input_df.loc[:, 'place_of_publication'] = input_df.replace(r' \[', ', ', regex=True).replace(']', '', regex=True)
    return input_df


def formatting_lists(df):
    input_df = df.copy()
    
    for col in input_df.columns:
        if list in list(set(input_df[col].apply(type).values)):
            if 'named_entities' not in col and 'parts_of_speech' not in col and 'sentence.' not in col and 'concordance.' not in col:
                print(f"{col} - {input_df.loc[input_df[col].str.len() == 1].shape[0]} rows have one {col.replace('_', ' ')}, {input_df.loc[input_df[col].str.len() > 1].shape[0]} rows have multiple {col.replace('_', ' ')}s, and {input_df.loc[input_df[col].str.len() == 0].shape[0]} rows have no {col.replace('_', ' ')}s")
            input_df.loc[input_df[col].apply(type) == list, col] = input_df[col].str.join('; ')
        
    return input_df


def title_cleaner(df):
    input_df = df.copy()

    input_df.loc[:, 'title_normal'] = input_df['title_normal'].str.strip('.').str.title().str.replace(' And ', ' and ').str.replace(' Of ', ' of ').str.replace(' The ', ' the ')
    input_df.loc[(input_df['title'].str.contains(r'The\s', case=False)) & (~input_df['title_normal'].str.contains(r', The$', case=False)), 'title_normal'] = input_df['title_normal'] + ', The'

    return input_df

def na_cleaner(df):
    input_df = df.copy()

    input_df.dropna(axis=1, how='all', inplace=True)
    
    for col in input_df.columns:
        if input_df[col].dtype == 'object':
            input_df[col].fillna("", inplace=True)
        elif input_df[col].dtype == 'int' or input_df[col].dtype == 'float':
            input_df[col].fillna(0, inplace=True)
    
    return input_df


def merging_search_terms(df):
    input_df = df.copy()
    search_terms = input_df.iloc[0]['search_terms'].replace('; ', ';').split(';')

    concordances = {term:[col for col in input_df.columns if col.lower().replace('-', ' ') == 'concordance.' + term] for term in search_terms}
    counts = {term:[col for col in input_df.columns if col.lower().replace('-', ' ') == 'counts.' + term] for term in search_terms}
    sentences = {term:[col for col in input_df.columns if col.lower().replace('-', ' ') == 'sentence.' + term] for term in search_terms}
    
    for term in search_terms:
        try: 
            input_df['concordance.'+term] = input_df[concordances[term]].agg(';'.join, axis=1)
            concordances[term].remove('concordance.'+term)
            input_df.drop(columns=concordances[term], inplace=True)

        except:
            pass
        
        try: 
            input_df['counts.'+term] = input_df[counts[term]].agg(sum, axis=1)
            counts[term].remove('counts.'+term)
            input_df.drop(columns=counts[term], inplace=True)
        except:
            pass

        try: 
            input_df['sentence.'+term] = input_df[sentences[term]].agg(';'.join, axis=1)
            sentences[term].remove('sentence.'+term)
            input_df.drop(columns=sentences[term], inplace=True)
        except:
            pass
    
    for column in [col for col in input_df.columns for term in search_terms if term in col and 'counts' not in col]:
        input_df[column] = input_df[column].str.replace(';;', '').str.strip(';')
    
    input_df = input_df.astype({col:'int64' for col in input_df.columns for term in search_terms if term in col and 'counts' in col})

    return input_df


def dataframe_cleaner(df):
    """Function to apply the datframe cleaning functions in one step"""
    input_df = df.copy()
    
    clean_df = (input_df
                .pipe(format_date)
                .pipe(reorder_columns)
                .pipe(standard_place_publication)
                .pipe(formatting_lists)
                .pipe(title_cleaner)
                .pipe(na_cleaner)
                .pipe(merging_search_terms))
                
    return clean_df


# For exporting to Excel
def export_excel(df: dict, 
                 filename: str = 'Chronicling America Search-[search terms].xlsx', 
                 sheet_name: str = 'search results'
                ):
    export_df = df.copy()
    
    params = df.iloc[0]['search_url'].split('?', 1)[1].replace('+', ' ').split('&')
    params_dict = {param.split('=')[0]: param.split('=')[1] for param in params}

    search_terms = ' '.join([key[:-4] + ' ' + val for (key, val) in params_dict.items() if re.search('(and|or|prox|phrase)text', key)])
    search_dates = '-'.join([val for (key, val) in params_dict.items() if re.search(r'^date\d$', key)])

    name = f"{search_terms} from {search_dates}".strip('phrase ').strip('and ').strip('or ').strip('prox ')

    if not re.search(r'\.xlsx$', filename):
        if re.search(r'\.[\d\w]{0,7}$', filename):
            filename = filename.rsplit('.', 1)[0].strip() + '.xlsx'
        elif re.search(r'^[\S\s]*$', filename):
            filename = filename.strip() + '.xlsx'

    if filename == 'Chronicling America Search-[search terms].xlsx':
        filename = filename.replace('[search terms]', name)
     
    try:
        if export_df['year'].max() < 1900:
            export_df.drop(columns='date', inplace=True)
        elif export_df['year'].max() >= 1900 and export_df['year'].min() < 1900:
            export_df.loc[export_df['year'] < 1900, 'date'] = ''
    except:
        print('Dates not formatted. Run dataframe function format_date() prior to export for formatted dates.')

    writer = pd.ExcelWriter(filename, engine_kwargs={'options': {
                                                'strings_to_numbers':  True,
                                                'strings_to_formulas': True,
                                                'strings_to_urls':     True
                                                }})
        
    export_df.to_excel(writer, 
                    sheet_name=sheet_name, 
                    index=False, na_rep='', 
                    engine='xlsxwriter', 
                    )

    workbook  = writer.book
    worksheet = writer.sheets[sheet_name]
    
    worksheet.freeze_panes(1, 0)
    
    center_format = workbook.add_format({
        'align': 'center', 
        'valign': 'vcenter',
        'text_wrap': True,   
    })

    left_format = workbook.add_format({
        'align': 'left', 
        'valign': 'vcenter',
        'text_wrap': True,   
    })

    ocr_format = workbook.add_format({
        'align': 'center', 
        'valign': 'top',
        'text_wrap': True,   
    })

    header_format = workbook.add_format({
        'text_wrap': True,
        'bold': True,
        'align': 'center',
        'valign': 'bottom'
    })


    for col_num, value in enumerate(export_df.columns.values):
        if re.match('concordance.', value):
            worksheet.write(0, col_num, value.replace('concordance.', 'Concordances of ').title(), header_format)
            worksheet.set_column(col_num, col_num, 65, center_format)

        elif re.match('sentence.', value):
            worksheet.write(0, col_num, value.replace('sentence.', 'Sentences containing ').title(), header_format)
            worksheet.set_column(col_num, col_num, 65, center_format)

        elif re.match('day|month|year|date order', value):
            worksheet.write(0, col_num, value.replace('_', ' ').title(), header_format)
            worksheet.set_column(col_num, col_num, 6, center_format, {'hidden': True})

        elif re.match('type|sequence|page$|counts|start_year|end_year|language$', value):
            worksheet.write(0, col_num, value.replace('counts.', 'Count of ').replace('_', ' ').title(), header_format)
            worksheet.set_column(col_num, col_num, 8, center_format)

        elif re.search('frequency|weekday', value):
            worksheet.write(0, col_num, value.title(), header_format)
            worksheet.set_column(col_num, col_num, 10, center_format)

        elif re.search('section_label|edition|edition_label|lccn', value):
            worksheet.write(0, col_num, value.replace('_', ' ').title(), header_format)
            worksheet.set_column(col_num, col_num, 12, left_format)
            
        elif re.search('city|county|state|country|batch', value):
            worksheet.write(0, col_num, value.title(), header_format)
            worksheet.set_column(col_num, col_num, 14, left_format)

        elif re.search('title_normal|place_of_publication|publisher|verbose date|id', value):
            worksheet.write(0, col_num, value.replace('_', ' ').title(), header_format)
            worksheet.set_column(col_num, col_num, 16, left_format)

        elif re.search('ocr_eng', value):
            worksheet.write(0, col_num, value.replace('ocr_eng', 'OCR Text (English)'), header_format)
            worksheet.set_column(col_num, col_num, 35, ocr_format)

        elif re.search('subject', value):
            worksheet.write(0, col_num, value.title(), header_format)
            worksheet.set_column(col_num, col_num, 50, left_format)

        elif re.search('note', value):
            worksheet.write(0, col_num, value.title(), header_format)
            worksheet.set_column(col_num, col_num, 100, left_format)

        else:
            worksheet.write(0, col_num, value.replace('_', ' ').title(), header_format)
            worksheet.set_column(col_num, col_num, 25, left_format)

    print('Created file:', filename)
    writer.save()

    if 'google.colab' in str(get_ipython()):
        from google.colab import files
        files.download(filename)       
[Installation](#installation) | 
[Searching the Chronicling America API](#search_options) | 
[Quick Start](#quickstart) | 
[Detailed Explaination](#detail) | 
[Importing the package](import) |
[Creating a Search Object](#search_object) | 
[Running a search on the Search Object](#run_search) | 
[Working with the dataframe](#dataframe) | 
[Saving and Retrieving](#saving_retrieving) | [Search Parameters](#search_parameters) | [Summary of data it's possible to retrieve](#results_summary) 

<br>

This package is for running searches on [Chronicling America](https://chroniclingamerica.loc.gov/), using the API, to then be able to run text analysis on the results, put the search results in a tabular form, and export to Excel.

<br>

<a id='installation'></a>
## Installation 

install via pip with: 
   ```
   pip install git+https://gitlab.unimelb.edu.au/mdap-public/chronicling-america.git
   ```

<br>

[Installation](#installation) | 
[Searching the Chronicling America API](#search_options) | 
[Quick Start](#quickstart) | 
[Detailed Explaination](#detail) | 
[Importing the package](import) |
[Creating a Search Object](#search_object) | 
[Running a search on the Search Object](#run_search) | 
[Working with the dataframe](#dataframe) | 
[Saving and Retrieving](#saving_retrieving) | [Search Parameters](#search_parameters) | [Summary of data it's possible to retrieve](#results_summary) 
<a id='search_options'></a>

## Searching the Chronicling America API

### Search Options
There are two types of searches that you can do with the API, these being: 
* **[Page](#page_search)**: A search of the Chronicling America database to find the text of articles etc. Finds individual newspaper pages that include the search term, and provides details of the newspaper, page number etc. 
  * *Search term* = "pages"
  
* **[Title](#title_search)**: A search of the US Newspaper Directory to see what newspapers are available. Uses a keyword search, looking through newspaper title, the essay about the paper, etc., and provides details of each newspaper such as place and dates of publication, publisher, county etc.
  * *Search term* = "titles"  
  
Currently the US Newspaper Directory has details of 157,520 newspapers from 1690-2022. It includes both newspapers that have been digitised and so are searchable on Chronicling America, and others that are only available in physical formats such as microfilm. As of January 2023, Chronicling America currently contains 20,166,086 pages from 3,820 newspapers, dated from 1777-1963. 

<br>

#### Important things to note: 
* When searching, common words (e.g. and, not, the) are ignored by the search engine
* Case is ignored 'fortune telling' and 'Fortune Telling' would get the same result
* Diacritic characters cause issues; unaccented characters should be used instead
* The search API (and the online search) does not recognise wildcard searches (using a character to sub in for a combination of letters). 
* However, it does automatically stem words, so searching 'fortune tell' would include telling, teller, tell- in the results; 'fortune teller' and 'fortune tellers' will have the same results (slight difference when testing). 
* Searches will still run if there are search parameters included but are blank, or if there are invalid search parameters (such as including a proximity value but searching the terms using andtext instead of proxText). The search will disregard those parameters (In the example given, will search for the words requested, ignoring the proximity value stated).
* You can use different text fields in the one search, however it will search for pages that contain both parameters. So, a search with terms for both 'phrasetext' and 'ortext' will produce pages that contain the phrase requested and any of the words in the 'ortext' parameter. 


For more information, see the [Chronicling America Search Help Guide](https://chroniclingamerica.loc.gov/help/)

<br>

[Installation](#installation) | 
[Searching the Chronicling America API](#search_options) | 
[Quick Start](#quickstart) | 
[Detailed Explaination](#detail) | 
[Importing the package](import) |
[Creating a Search Object](#search_object) | 
[Running a search on the Search Object](#run_search) | 
[Working with the dataframe](#dataframe) | 
[Saving and Retrieving](#saving_retrieving) | [Search Parameters](#search_parameters) | [Summary of data it's possible to retrieve](#results_summary) 
<a id='quickstart'></a>
## Quickstart

### 1. Import the package and functions
import Chronicling America Search into your notebook:

```
import chron_am_search
from chron_am_search import search_functions
```

<br>

### 2. Create a Search Object

Create a Search Object, including your search parameters: 

```
search_name = Search(param1='this', param2='that', param3=['here', 'there', 'everywhere'], param4='1900'))
```

<br>

It is recommended to also run the ```result_count()``` method the first time you create a search, before the next step, to check the size of the search results. 

```
search_name.result_count
```

<br>

### 3. Search Results to Dataframe

Run ```Search.search_to_frame()``` to compile each page of the search results, and output to a dataframe. The function includes a default dataframe cleaning and formatting process - include the parameter ```clean=False``` to disable this. 

```
df = search_name.search_to_frame()
```


<br>


### 4. Export to Excel

To save a copy of your search results or continue working in Excel (characters contained in the OCR search results cause errors if exporting to CSV)

```
export_excel(df)
```

<br>

### Example
```
import chron_am_search
from chron_am_search import search_functions

searching_newspapers = Search(
                              phrasetext='reading newspaper', 
                              andtext='hunting', 
                              dateFilterType='yearRange', 
                              date1='1800', 
                              date2='1860'
                            )

searching_newspapers.result_count

df_news = searching_newspapers.search_to_frame()

export_excel(df_news)
```

<br>

[Installation](#installation) | 
[Searching the Chronicling America API](#search_options) | 
[Quick Start](#quickstart) | 
[Detailed Explaination](#detail) | 
[Importing the package](import) |
[Creating a Search Object](#search_object) | 
[Running a search on the Search Object](#run_search) | 
[Working with the dataframe](#dataframe) | 
[Saving and Retrieving](#saving_retrieving) | [Search Parameters](#search_parameters) | [Summary of data it's possible to retrieve](#results_summary) 
<a id='detail'></a>
## Detailed explainations

<a id='import'></a>
### Importing Chronicling America Search
to begin using chron_am_search, you will need to import both the package and the search functions. This is done by running: 

```
import chron_am_search
from chron_am_search import search_functions
```
at the beginning of your notebook. 


<br>

<a id='search_object'></a>
### Creating a Search Object

Give the search a name (no spaces), and create a search object using ```Search()```. In the brackets, include all the parameters for your search, with a comma between them. ```Search()``` defaults to being a page search: 
```
search_name = Search(param1='this', param2='that', param3=['here', 'there', 'everywhere'], param4='1900')
```
<br>

If you are wanting to run a title search, include ```search_type='title'``` before the search parameters:
```
title_search_name = Search(search_type='title', param1='this', param2='that', param3=['here', 'there', 'everywhere'], param4='1900')
```

Examples:
```
searching_newspapers = Search(
                              phrasetext='reading newspaper', 
                              andtext='hunting', 
                              dateFilterType='yearRange', 
                              date1='1800', 
                              date2='1860'
                            )
```
A list of all the [search perameter options](#search_parameters) is available at the end of this page. 

<br>

## Search Object Properties
The search object created also has properties that you can view, including:
* **search_type**: if the search object was a page or title search
* **params**: the parameters set
* **search_url**: The url of the search, to view the results as a JSON object in the browser
* **search_terms**: All the search terms included in the search, regardless of which parameter was used
* **first_page**: the first page of results (only available after search has been run)
* **data_dict**: all the results in a dictionary (only available after gather_results() or search_to_frame() has been run)

To use these functions, type the name of your search object, period, property name:

```
search_name.search_type
```
```
searching_newspapers.search_type
```
```
searching_newspapers.params
```

Some properties, such as ```search_url```, ```search_terms```, and ```first_page``` will be blank until a search is run on the search object. 

<br>

[Installation](#installation) | 
[Searching the Chronicling America API](#search_options) | 
[Quick Start](#quickstart) | 
[Detailed Explaination](#detail) | 
[Importing the package](import) |
[Creating a Search Object](#search_object) | 
[Running a search on the Search Object](#run_search) | 
[Working with the dataframe](#dataframe) | 
[Saving and Retrieving](#saving_retrieving) | [Search Parameters](#search_parameters) | [Summary of data it's possible to retrieve](#results_summary) 
<a id='run_search'></a>
### Running a search on the Search Object

Once the search object has been created, you can run a search on it to get the information you are looking for. There is one overall function that will do this, and create a dataframe as an output, ```search_to_frame()```. This is made up of an number of different methods that you can run individually. 

<br>

#### Methods for the Search Object

* **```result_count()```**: See how many results are included in the search
* **```get_link()```**: Get the URL to view the search on the Chronicling America website
* **```gather_results()```**: Run the search, with the results being outputted as a list of JSON objects
* **```search_to_frame()```**: Run the search, with the results being outputted as a Pandas dataframe
* **```dataframe_cleaner()```**: Applies functions to clean the dataframe (discussed below)
* **```list_newspaper_lccns()```** List the Newspaper LCCN numbers (title search only)
* **```run_search()```**: Run the search, with the results being outputted as a JSON object of the first page.
    * (note: these listed functions all include `run_search()`, so you can use them without having to use `run_search()` first). 

There are also other functions included above, which are there to be used inside these functions, e.g. `checking params()` prints out if there are any identified issues with the parameters included in the search. 

<br>

To use these functions, type the name of your search object, period, function name (with brackets):
```
search_name.result_count()
```
e.g.:

```
searching_newspapers.result_count()
```
```
searching_newspapers.get_link()
```

<br>

#### Chronicling America API Formatting

When you run a search, the results come back from Chronicling America as a JSON object, and are paginated. The default amount of results per page from the API is 20; in the functions created it is 50. If you want to change the number of results per page, you can include the parameter ```rows``` when creating your search object:
```
search_name = Search(param1='this', param2='that', rows=100)
```

<br>

If you want to see what the first page of your search results looks like, call the attribute ```first_page```:
```
search_name.firstpage
```
e.g.:
```
searching_newspapers.first_page
```

<br>

#### Gathering the Search Results
The function ```gather_results()``` compiles all the pages of results into a single list. The function ```search_to_frame()``` takes this list (running the function if it hasn't already been run) and puts it in a dataframe, or a table format. 

When using these functions, you should create a variable:
```
dictionary_name = search_name.gather_results()
```
This will assign the result of the function to the variable. That way you can call the result again without having to re-run the function.

<br>

A common practice is to call a dataframe df. If you have different searches in the same notebook, however, you should prefix or suffix the df with a meaningful name. Using the same name for two dataframes means the second one will overwrite the first. 

```
news_search = searching_newspapers.gather_results()
```

<br>

* **```image_links```**: adds columns with links to download a jpg2000 image of the newspaper page, download a pdf of the newspaper page, or view the newspaper page on the Chronicling American website. 
    * It's possible to access these links by following JSON links. However Chronicling America uses standard formatting, so that it is faster to create the links in the dataframe. 
    * If you do not want the image links included in the dataframe, set the parameter ```links=False```, e.g. 
    ```
    news_search = searching_newspapers.gather_results(links=False) 
    ```

<br>

#### Natural Language Processing (NLP)

Included in the function ```gather_results()``` is the function ```apply_nlp()``` which uses the [Spacy](https://spacy.io/) to do some natural language processing on the search results. 

The processing included in the function is: 
* **word tokenization**: Splitting the OCR text so that each word or punctuation mark is an individual token (results not included in the output)
* **sentence tokenization**: Splitting the OCR text so that each sentence is an individual token (results not included in the output)
* **count**: counting how many times the search terms are included in each result (including breaking down individual words from a phrase) 
* **concordance**: putting search terms in context, showing 5 words or punctuation on either side
* **sentence context**: Listing the sentences that phrases appear in. 
* **named entity recognition**: Listing the words that Spacy has identified as a named 'real world object', such as a person, organisation, or place. These are split into different types and sorted alphabetically. 
* **parts of speech**: Identifying the linguistic part of speech for each token in the OCR text. The default settings returns a list of identified proper nouns. 

**Important note re natural language processing**: As noted earlier, the Chronicling America search ignores case, allows for a hyphen between words, and also lemmatizes the words, e.g. searching for 'telling' may also return results with 'tell' or 'teller'. 

The natural language processing applied to the search results finds exact matches only. As such, it has been set to search for all the terms in upper case, lower case, title case, and capitalised, and with a hyphen between words. However, it currently does not include searching for the lemmatization of the word, or the possible alternative endings. 

<br>

If you do not want the natural language processing to be run at all, include the parameter ```nlp=False``` in either ```gather_results()``` or ```search_to_frame()```.

You also have the option to turn off or change what is returned: 

| nlp option | default | how to change |
| ---------- | ------- | ------------- |
| *concordance* | concord=True | to turn off, include the argument concord=False |
| *sentence context* | sentence=True | to turn off, include the argument sentence=False |
| *named entity recognition* | ner=True | to turn off, include the argument ner=False |
| *parts of speech identification* | pos=True | to turn off, include the argument pos=False |
| *pos types that are reurned* | pos_choice=['PROPN'] | change or add the options included in the pos_choice argument list, e.g. pos_choice=['ADJ', 'NOUN', 'PRON'] |

For example, to turn off sentences and named entity recognition, keep concordance, and change parts of speech to include adjectives, nouns, and pronouns:
```
news_search = searching_newspapers.gather_results(
                                                  sentence=False, 
                                                  ner=False, 
                                                  pos_choice=['ADJ', 'NOUN', 'PRON']
                                                )
```

Available part of sppech options are: 
| POS | DESCRIPTION |
|---- | ----------- | 
| ADJ | adjective |
| ADP | adposition |
| ADV | adverb< |
| AUX | auxiliary |
| CONJ | conjunction |
| CCONJ | coordinating conjunction |
| DET | determiner |
| INTJ | interjection |
| NOUN | noun |
| NUM | numeral |
| PART | particle |
| PRON | pronoun |
| PROPN | proper noun |
| PUNCT | punctuation |
| SCONJ | subordinating conjunction |
| SYM | symbol |
| VERB | verb |
| X | other |
| SPACE | space |


<br>

[Installation](#installation) | 
[Searching the Chronicling America API](#search_options) | 
[Quick Start](#quickstart) | 
[Detailed Explaination](#detail) | 
[Importing the package](import) |
[Creating a Search Object](#search_object) | 
[Running a search on the Search Object](#run_search) | 
[Working with the dataframe](#dataframe) | 
[Saving and Retrieving](#saving_retrieving) | [Search Parameters](#search_parameters) | [Summary of data it's possible to retrieve](#results_summary) 
<a id='dataframe'></a>
### Working with the dataframe

The ```search_to_frame()``` function also formats the results into a Pandas dataframe (a tabular format). If you have already run ```gather_results()```, you can still run the ```search_to_frame()``` function, and it will use the data already gathered, rather than running the search again. 

```search_to_frame``` also includes the function ```dataframe_cleaner()```, which runs some standardised cleaning and formatting on the dataframe. If you do not wan't this to be included, set the parameter ```clean=False```. 

```dataframe_cleaner()``` is itself made up of a series of functions, which you can also run individually on your dataframe. 

```
df_news = searching_newspapers.search_to_frame(clean=False)
df_news.head(2)
```

<br>

#### Dataframe Cleaner Functions
Because of how the data is supplied from the API are some aspects to the dataframe that you may want to tidy, in order to be easier to use. Some functions to help with this include: 
* **```format_date```**: reformat the date from YYYYMMDD (no deliminators) to a datetime object. 
    * Default is for it to also split the date, adding columns year, month, day, and weekday, and date order. These are needed when exporting to Excel if results include dates before 1 January, 1900. To not include the date split columns, set the parameter ```date_split=False```

* **```reorder_columns```**: Re-orders the columns to group similar information together.
    * new column order is: 
        * Introduction columns: 'title_normal', 'place_of_publication', 
        * Date columns: 'verbose date', 'date', 'weekday', 'day', 'month', 'year', 'date order', 
        * Count and concordance columns: [columns for the count of each word], [columns for the concordance for each word], [columns for the phrase sentences], 
        * Text columns: 'ocr_eng', 'sentences', 
        * Issue columns: 'sequence', 'page', 'section_label', 'edition', 'edition_label',
        * Newspaper columns: 'title', 'alt_title', 'frequency', 'place', 'city', 'county', 'state', 'country', 'start_year', 'end_year', 'publisher', 'lccn', 'language', 
        * Text detail columns: 'tokens', 'subject', 'note', 'type'
        * Metadata columns: 'batch', 'id', 'url'

    * then any additional columns e.g. ocr in a different language. 
    * Default is set to not include word tokens - to change this, set parameter ```tokens=True```

* **```standard_place_publication```**: remove square brackets included around some state abbreviations
* **```formatting_lists```**: updates columns such as alt_title, place, city, county, and state where the results are given as a list, rather than a string. Removes lists, except where there are multiple values for the row, remove duplicates, and replaces None values with empty strings
    * default for columns alt_title, place, city, county, state, and language is to have data in a list, even if only one result. city, county, and state are linked, so that if a result has two city names, the county and state will be listed twice as well. 
* **```title_cleaner```**: Formats the newspaper titles in the column 'title_normal' so that they are in title case rather than lower case. If the version of the title in column 'title' starts with 'The', adds ', The' to to the end of the title in the column 'title_normal'. 
* **```na_cleaner```**: change N/A resuts to be an empty string or a 0, and remove any columns where all the rows are N/A
* **```merging_search_terms```**: the dataframe will initially output all the different formatting of a search term into it's own column, this merges them together. 
    
* **```dataframe_cleaner```**: runs the functions ```format_date()```, ```reorder_columns()```, ```standard_place_publication()```, ```formatting_lists()```, ```title_cleaner()``` ```na_cleaner```, and ```merging_search_terms()``` in a single step. 

To run these functions, put your dataframe as a parameter.
```
df_news = format_date(df_news)
df_news.head(2)
```
```
df_news = reorder_columns(df_news)
df_news.head(2)
```
```
df_news = standard_place_publication(df_news)
df_news.head(2)
```
```
df_news = title_cleaner(df_news)
df_news.head(2)
```
```
df_news = formatting_lists(df_news)
df_news.head(2)
```

<br>

[Installation](#installation) | 
[Searching the Chronicling America API](#search_options) | 
[Quick Start](#quickstart) | 
[Detailed Explaination](#detail) | 
[Importing the package](import) |
[Creating a Search Object](#search_object) | 
[Running a search on the Search Object](#run_search) | 
[Working with the dataframe](#dataframe) | 
[Saving and Retrieving](#saving_retrieving) | [Search Parameters](#search_parameters) | [Summary of data it's possible to retrieve](#results_summary) 
<a id='saving_retrieving'></a>
## Saving and Retrieving

### File Format
It is usually best practice when saving or sharing tabular data to save it in a plain text format such as .csv. However, the Chronicling America OCR text results can be quite messy and include a combination of commas, single quotation marks and double quotation marks, which can cause ambiguity with the data in a CSV file and result in the data becoming unstructured. 

Therefore, for this data using a format such as Excel is preferred. 

<br>

### Saving To Excel
For every search you want to conduct, you can create a new search object, and as long as they have a unique name, you will be able to easily re-run that search. However, searches with large numbers of results can take a long time to run, so it's a good idea after you have formatted the search as a dataframe to export it as a file. This way, you will be able to load a dataframe of results without having to run the search again. 

Exporting the dataframe to Excel or a similar spreadsheet program can also be helpful for viewing and using the data. 

Use the function ```export_excel()``` to export the dataframe. This includes some formatting for easier reading of the Excel spreadsheet, including: 
* **Default filename**: Creates a filename 'Chronicling America Search-[your search terms] [search from date]-[search to date].xlsx'.
    * to set a different filename, include the parameter ```filename='your filename'```
* **Default sheetname**: Creates a sheet name 'search results'. 
    * to set a different sheet name, include the parameter ```sheet_name='your sheet name'```
* **File extension correction**: corrects the file extention to .xlxs if extention missing or for a different file format
* **Removes the date column if all dates before 1900**: Excel does not recognise dates prior to 1900 as dates. Keeping these in the spreadsheet will cause errors. 
* **Sets appropriate column width, wrapping and alignment**


It will save in the same location as the notebook you are using. 
```
export_excel(df_news)
```

<br>

### Retrieving from Excel

To take an Excel file you had previously saved and turn it to a dataframe, use the function ```pd.read_excel()```. Create a variable name for the dataframe you are creating, and in the brackets include the name of the file as a string (in quotation marks). If the file is stored in a different folder than the notebook you are using, you will also need to include the filepath in the string: 
```
df = pd.read_excel('folder/filename.xlsx')
```
For more information, go to https://pandas.pydata.org/docs/reference/api/pandas.read_excel.html

```
df = pd.read_excel('Chronicling America Search-ing newspaper and hunting from 1800-1860.xlsx')
df.head(2)
```

*Note: need to create a function to import .xlsx and return date column if missing

<br>

[Installation](#installation) | 
[Searching the Chronicling America API](#search_options) | 
[Quick Start](#quickstart) | 
[Detailed Explaination](#detail) |
[Importing the package](import) | 
[Creating a Search Object](#search_object) | 
[Running a search on the Search Object](#run_search) | 
[Working with the dataframe](#dataframe) | 
[Saving and Retrieving](#saving_retrieving) | [Search Parameters](#search_parameters) | [Summary of data it's possible to retrieve](#results_summary) 
<a id='search_parameters'></a>
## Search Parameters

### Chronicing America Page Search 
The available search parameters when searching the Chronicling America database are:
* **andtext**: find all of the given words
  * *Entered as:* a string (within quotation marks); words seperated by space only, no commas
  
* **ortext**: find any of the given words
  * *Entered as:* a string (within quotation marks); words seperated by space only, no commas
  * *Available options:* All 50 US States, District of Columbia, American Samoa, Mariana Islands, Puerto Rico, Virgin Island
  
* **phrasetext**: find the given words as a phrase.
  * *Entered as:* a string (within quotation marks); words seperated by space only, no commas
  * *Note* - It will still search the words in any order, as long as they are together, but the results with the order you provide will come first. 
  
* **proxText**: find the given words within a specified proximity (must also give proximity value)
  * *Entered as:* a string (within quotation marks)
  
* **proximityValue**: how close together the words in a proximity search need to be (only applies to words in the proxText option)
  * *Entered as:* a string (within quotation marks)
  * *Default:* 1690
  
* **dateFilterType**: choosing the date format when searching within a date range
  * *Entered as:* a string (within quotation marks)
  * *Available options:* range (to search mm/dd/yyyy), yearRange (to search yyyy)
  
* **date1**: from date (year or mm/dd/yyyy) when searching within a date range
  * *Entered as:* a string (within quotation marks)
  
* **date2**: to date (year or mm/dd/yyyy) when searching within a date range
  * *Entered as:* a string (within quotation marks)
  
* **state**: list of states wanting to include (leave blank for all states)
  * *Entered as:* a string (within quotation marks) if one state; a list of strings (within square brackets) for multiple states
  
* **lccn**: list of newspapers wanting to limit the search to (leave blank for all newspapers)
* *Entered as:* a string (within quotation marks) containing the newspaper LCCN for one newspaper; a list of strings (within square brackets), each containing a newspaper LCCN, for multiple states
  * *Note* - can get a correctly formatted list of LCCNs by running a title search for the newspapers you want, using the [list_newspaper_lccns() function](#lccn_list)
  
* **sequence**: Image sequence number for the issue of the paper being searched - Most often corresponds with the page number. For example, to search only for results appearing on the front page of a newspaper, sequence number should be 1. 
  * *Entered as:* a string (within quotation marks)


<br>

### US Newspaper Directory Title Search 
The available search parameters when searching the US Newspaper Directory are:
* **terms**: the words that you want to search. Searches for term in title, subject, notes (possibly all fields) 
  * *Entered as:* a string (within quotation marks)
  
* **state**: The state the desired newspaper is located in. Can only search one state at a time. 
  * *Entered as:* a string (within quotation marks)
  * *Available options:* All 50 US States, District of Columbia, American Samoa, Mariana Islands, Puerto Rico, Virgin Island
  
* **county**: The county the desired newspaper is located in. Can only search one county at a time. 
  * *Entered as:* a string (within quotation marks)
  
* **city**: The city the desired newspaper is located in. Can only search one city at a time. 
  * *Entered as:* a string (within quotation marks)
  
* **year1**: Searching for newspapers published within a specific year range - from year. 
  * *Entered as:* a string (within quotation marks)
  * *Default:* 1690
  
* **year2**: Searching for newspapers published within a specific year range - to year. 
  * *Default:* 2022
  
* **frequency**: Frequency of publication
  * *Entered as:* a string (within quotation marks)
  * *Available options:* Daily, Semiweekly, Three times a week, Weekly, Biweekly, Monthly, Semimonthly, Three times a month, Other, Unknown
  
* **language**: available languages to search
  * *Entered as:* a string (within quotation marks)
  
* **ethnicity**: ethnicity of the target readership
  * *Entered as:* a string (within quotation marks)
  
* **labor**: industry or movement of the target readership
  * *Entered as:* a string (within quotation marks)
  
* **material_type**: available format of the newspaper
  * *Entered as:* a string (within quotation marks)
  * *Available options:* Microfilm Master, Microfilm Print Master, Microfilm Service Copy, Microform, Online Resource, Original, Unspecified
  
* **lccn**: Library of Congress Control Number for specific newspaper
  * *Entered as:* a string (within quotation marks)

<br>

[Installation](#installation) | 
[Searching the Chronicling America API](#search_options) | 
[Quick Start](#quickstart) | 
[Detailed Explaination](#detail) | 
[Importing the package](import) |
[Creating a Search Object](#search_object) | 
[Running a search on the Search Object](#run_search) | 
[Working with the dataframe](#dataframe) | 
[Saving and Retrieving](#saving_retrieving) | [Search Parameters](#search_parameters) | [Summary of data it's possible to retrieve](#results_summary) 
<a id='results_summary'></a>
## Summary of data it's possible to retrieve

Below is a summary of what data is included in the results after a search. In some instances, the results include links to other JSON objects, with more/different data available. These are all included below. 

### Page Search Results
```
awardee_example_url =     'https://chroniclingamerica.loc.gov/awardees/idhi.json'
batch_level_example_url = 'https://chroniclingamerica.loc.gov/batches/idhi_galapagos_ver01.json'
paper_level_example_url = 'https://chroniclingamerica.loc.gov/lccn/sn86063039.json'
issue_level_example_url = 'https://chroniclingamerica.loc.gov/lccn/sn86063039/1923-12-07/ed-1.json'
page_level_example_url  = 'https://chroniclingamerica.loc.gov/lccn/sn86063039/1923-12-07/ed-1/seq-11.json'
page_search_example_url = 'https://chroniclingamerica.loc.gov/search/pages/results/?andtext=fortune+telling&format=json&rows=20&page=1'

page_search_data = {
    'totalItems': 'A count of the number of results in a search', 
    'endIndex': 'index number of the last item on this page', 
    'startIndex': 'index number of the first item on this page', 
    'itemsPerPage': 'number of items from the search result appearing on each page', 
    'items': { 
         'sequence': 'image sequence number for that issue, usually corresponding to the issue page number.',
         'county': ['state county for the city the newspaper was published in', 'past and present county names']
         'edition': 'edition number for that date (e.g. 1)',
         'frequency': 'publication frequency for the newspaper the page is from',
         'id': 'persistent link for html access to this newspaper page (without the prefix https://chroniclingamerica.loc.gov)',
         'subject': ['relevant Library of Congress subject categories for the newspaper'],
         'city': ['City the newspaper was published in', 'past and present city names'],
         'date': 'date of the issue, specified as yyyy-mm-dd (e.g. 1902-01-30)',
         'title': 'Title of the newspaper',
         'end_year': 'year newspaper ceased publication',
         'note': 'Notes about the newspaper, such as additional publication details or Library holdings.',
         'state': ['state for the city the newspaper was published in', 'past and present state names']
         'section_label': 'section of the newspaper the page appears in',
         'type': 'type of search result - will be page',
         'place_of_publication': 'City and state the newspaper was published in',
         'start_year': 'year newspaper began publication',
         'edition_label': 'name of edition the page from, e.g. "last edition", "Noon edition",
         'publisher': 'Publisher of the newspaper',
         'language': 'language newspaper printed in',
         'alt_title': 'Alternative titles for the newspaper',
         'lccn': 'Library of Congress Control Number for the newspaper the page is from',
         'country': 'Country the newspaper published in - those published in the United States will list the state',
         'ocr_eng': 'the OCR text (in English) of the subset of the page where the search words were found',
         'batch': 'The name of the batch of files this page was part of that were uploaded to Chronicling America',
         'title_normal': 'Standard title of the newspaper',
         'url': 'link to ***page_level_data***, this being a JSON object with additional page details, including image links', 
         'place': ['location details combined together, as State--County--City', 'past and present name combinations'], 
         'page': 'page number (e.g. if different from the sequence number)'
     }
}

page_level_data = {
    'jp2': 'link for JPG2000 image of the page',
    'sequence': 'image sequence number for that issue, usually corresponding to the issue page number.',
    'text': 'link for the OCR text (in English) of the subset of the page where the search words were found',
    'title': {
        'url': 'link to ***paper_level_data***, this being a JSON object with newspaper details and links to ***issue_level_data*** for each issue of this newspaper',
        'name': 'Title of the newspaper'
    },
    'pdf': 'link for pdf image of the page',
    'ocr': 'link for XML data for generating OCR data',
    'issue': {
        'url': 'link to ***issue_level_data***, this being a JSON object with newspaper issue details and links to ***page_level_data*** for each page of the issue',
        'date_issued': 'date of the issue, specified as yyyy-mm-dd (e.g. 1902-01-30)'
    }
}  
    
issue_level_data = {
    'title': {
        'url': 'link to ***paper_level_data***, this being a JSON object with newspaper details, and links to ***issue_level_data*** for each issue of this newspaper',
        'name': 'Title of the newspaper'
    }, 
    'url': 'link to ***issue_level_data*** (this object), 
    'date_issued': 'date of the issue, specified as yyyy-mm-dd (e.g. 1902-01-30)', 
    'number': 'issue number for this issue', 
    'batch': {
        'url': 'link to ***batch_level_data*** this being a JSON object with details, of the batch of files this page was part of that were uploaded to Chronicling America and links to ***issue_level_data*** for each issue included in this batch', 
        'name': 'The name of the batch of files this page was part of that were uploaded to Chronicling America'
    }, 
    'volume': 'Issue volume number', 
    'edition': 'Edition number', 
    'pages': [# List of dictionaries for each page of this issue, providing:
        {'url': 'link to ***page_level_data***, this being a JSON object with additional page details, including image links', 
        'sequence': 'image sequence number for this issue, usually corresponding to the issue page number.'}, 
    ]
}   

paper_level_data = {
    'place_of_publication': 'City and state the newspaper was published in', 
    'lccn': 'Library of Congress Control Number for the newspaper the page is from', 
    'start_year': 'year newspaper began publication', 
    'place': ['location details combined together, as State--County--City', 'past and present name combinations'], 
    'name': 'Title of the newspaper', 
    'publisher': 'Publisher of the newspaper', 
    'url': 'link to ***paper_level_data*** (this object)', 
    'end_year': 'year newspaper ceased publication', 
    'issues': [# List of dictionaries for each issue of this newspaper, providing:
        {'url': 'link to ***issue_level_data***, this being a JSON object with newspaper issue details and links to ***page_level_data*** for each page of the issue', 
        'date_issued': 'date of the issue, specified as yyyy-mm-dd (e.g. 1902-01-30)'},
    ], 
    'subject': ['relevant Library of Congress subject categories for the newspaper']
}


batch_level_data = {
    'name': 'The name of the batch of files this page was part of that were uploaded to Chronicling America', 
    'url': 'link to ***batch_level_data*** (this object)', 
    'page_count': 'number of files in the batch', 
    'awardee': {
        'url': 'link to ***awardee_level_data***, this being a JSON object with digitisation organisation details and links to ***batch_level_data*** for each batch they provided', 
        'name': 'Name of the organisation who provided the batch'
    }, 
    'lccns': ['Library of Congress Control Numbers for the newspapers the issues in the batch are from'], 
    'ingested': 'date/time the batch ingested into the Library of Congress-Chronicling America', 
    'issues': [ # List of dictionaries for each issue of this batch, providing:
        {'url': 'link to ***issue_level_data***, this being a JSON object with newspaper issue details and links to ***page_level_data*** for each page of the issue', 
         'date_issued': 'date of the issue, specified as yyyy-mm-dd (e.g. 1902-01-30)', 
         'title': {
             'url': 'link to ***paper_level_data***, this being a JSON object with newspaper details and links to ***issue_level_data*** for each issue of this newspaper', 
             'name': 'Title of the newspaper',
         }
        }, 
    ]
}

awardee_data = {
    'url': 'link to ***awardee_level_data*** (this object)', 
    'batches': [ # List of dictionaries for each batch from this organisation, providing:
        {'url': 'link to ***batch_level_data*** this being a JSON object with details, of the batch of files this page was part of that were uploaded to Chronicling America and links to ***issue_level_data*** for each issue included in this batch', 
         'name': 'The name of the batch of files this page was part of that were uploaded to Chronicling America'},
    ], 
    'name': 'Name of the organisation'
}

### Title Search Results
title_search_data = {
    'totalItems': 'A count of the number of results in a search',
    'endIndex': 'index number of the last item on this page', 
    'startIndex': 'index number of the first item on this page', 
    'itemsPerPage': 'number of items from the search result appearing on each page', 
    'items': { 
                 'essay': 'A descriptive essay about the newspaper and its history', 
                 'place_of_publication': 'City and state the newspaper was published in', 
                 'start_year': 'year newspaper began publication', 
                 'publisher': 'Publisher of the newspaper', 
                 'county': ['state county for the city the newspaper was published in', 'past and present county names'], 
                 'edition': 'edition name for newspapers with different editions', 
                 'frequency': 'publication frequency for the newspaper', 
                 'url': 'link to ***paper_level_data***, this being a JSON object with newspaper details and links to ***issue_level_data*** for each issue of this newspaper', 
                 'id': 'persistent link for html access to this newspaper (without the prefix https://chroniclingamerica.loc.gov)', 
                 'subject': ['relevant Library of Congress subject categories for the newspaper'],
                 'city': ['City the newspaper was published in', 'past and present city names'],
                 'language': 'language newspaper printed in', 
                 'title': 'Title of the newspaper', 
                 'holding_type': ['materials that the newspaper is available in', 'e.g. Online Resource, Microfilm'], 
                 'end_year': 'year newspaper ceased publication', 
                 'alt_title': 'Alternative titles for the newspaper', 
                 'note': 'Notes about the newspaper, such as additional publication details or Library holdings.', 
                 'lccn': 'Library of Congress Control Number for the newspaper', 
                 'state': ['state for the city the newspaper was published in', 'past and present state names'], 
                 'place': ['location details combined together, as State--County--City', 'past and present name combinations'], 
                 'country': 'Country the newspaper published in - those published in the United States will list the state', 
                 'type': 'type of search result - will be title', 
                 'title_normal': 'Standard title of the newspaper', 
                 'oclc': 'Online Computer Library Center number'
             }
            }
```
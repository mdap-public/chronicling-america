import pytest
from chron_am_search import __version__
from chron_am_search.search_functions import *


def test_version():
    assert __version__ == '0.1.0'

@pytest.fixture
def example_page_and_search():
    return [
        {
            "andtext": "bananas cats",
            "dateFilterType": "yearRange",
            "date1": "1800",
            "date2": "1899",
            }
        ]

@pytest.fixture
def example_page_or_search():
    return [
        {
            "ortext": "bananas cats",
            "dateFilterType": "yearRange",
            "date1": "1800",
            "date2": "1899",
            }
    ]

@pytest.fixture
def example_page_phrase_search():
    return [
            {
                "phrasetext": "cat is bananas",
                "dateFilterType": "yearRange",
                "date1": "1900",
                "date2": "1963",
            }
    ]

@pytest.fixture
def example_page_proximity_search():
    return [
            {
                "proxText": "bananas cat",
                'proximityValue': '5',
                "dateFilterType": "yearRange",
                "date1": "1900",
                "date2": "1963",
            }
    ]

@pytest.fixture
def page_and_class():
    return Search(params=example_page_and_search)



def test_type(page_and_class):
    assert page_and_class.search_type == 'page'

def test_url_path(page_and_class):
    assert page_and_class.url_path == "https://chroniclingamerica.loc.gov/search/pages/results/"

def test_terms(page_and_class):
    if page_and_class.search_url == "":
        assert page_and_class.search_terms == []
    else:    
        assert page_and_class.search_terms == ['bananas', 'cats']